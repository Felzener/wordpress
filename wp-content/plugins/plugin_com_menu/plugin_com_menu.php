<?php

/*
* Plugin Name: Menu Admin
* Plugin URI: https://bitbucket.org/Felzener
* Description: Exemplo de como trabalhar com menu do Admin.
* Version: 0.0.1
* Author: Sergio Felzener
* Author URI: https://bitbucket.org/Felzener
* License: CC
*/

add_action('admin_init', 'configs_plugin_menu');

function configs_plugin_menu(){

    register_setting('configs-plugin-menu','url-api-auth');
    register_setting('configs-plugin-menu','url-api-endpoint1');
    register_setting('configs-plugin-menu','url-api-token');

}


add_action('admin_menu', 'new_menu_item');

function new_menu_item()
{
    // adiciona um item no menu
    /*
    add_menu_page(
        'Configurações do plugin menu',
        'Config Plugin Menu',
        'administrator',
        'config-plugin-menu',
        'open_config_plugin_menu',
        'dashicons-hammer'
    );
    */

    // Para criar um Sub-Menu de um item no menu 
    add_submenu_page('options-general.php', 
                     'Config Plugin Menu',
                     'Config Plugin Menu',
                     'administrator',
                     'config-plugin-menu',
                     'open_config_plugin_menu',
                     1);
}   

function open_config_plugin_menu(){
    require 'plugin_menu_configs_view.php';
}
