<?php

/*
* Plugin Name: Criar item menu Admin e Salvar Config Versao POO
* Plugin URI: https://bitbucket.org/Felzener
* Description: Exercicio em Aula Criando Item no Menu e Salvando configurações Versao POO.
* Version: 0.0.1
* Author: Sergio Felzener
* Author URI: https://bitbucket.org/Felzener
* License: CC
*/


//evita que o codigo seja executado se for chamado diretamente
if ( !defined ( 'WPINC' )){
     die;
}

class PluginPoo {

    function __construct() {
        add_action('admin_init', array($this , 'add_contact'));
        add_action('admin_menu', array($this, 'new_contact'));
    }

    function add_contact(){

        register_setting('add-contact','add-contact-name-poo');
        register_setting('add-contact','add-contact-email-poo');
        register_setting('add-contact','add-contact-website-poo');

    }


    function new_contact(){
        // adiciona um item no menu
        /*
        add_submenu_page('options-general.php', 
                         'Configurações add New Contact',
                         'Configurações add New Contact',
                         'administrator',
                         'config-plugin-menu',
                        'open_add_contact',
                        1);
        */
        add_menu_page(
            'Configurações add New Contact',
            'Add Contact',
            'administrator',
            'add-contact',
            array($this,'open_add_contact'),
            'dashicons-email-alt'
        );
}



    function open_add_contact(){
        require 'plugin_poo_config.php';
    }

   

}
    
new PluginPoo ();
