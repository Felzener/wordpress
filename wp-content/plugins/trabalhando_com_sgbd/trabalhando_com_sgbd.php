<?php 

/*
* Plugin Name: Trabalhando com SGBD
* Plugin URI: https://bitbucket.org/Felzener
* Description: Trabalhando com Sistema de Gerenciamento de Banco de Dados.
* Version: 0.0.1
* Author: Sergio Felzener
* Author URI: https://bitbucket.org/Felzener
* License: CC
*/

add_action('admin_menu', 'grava_db');

function grava_db()
{
    // adiciona um item no menu
    
    add_menu_page(
        'Configurações do plugin menu',
        'Grava DB',
        'administrator',
        'config-grava-db',
        'open_config_plugin_menu_sgbd',
        'dashicons-database-add'
    );
    

    // Para criar um Sub-Menu de um item no menu 
    /*
    add_submenu_page('options-general.php', 
                     'Gravar',
                     'Gravar',
                     'administrator',
                     'config-plugin-menu',
                     'open_config_plugin_menu_sgbd',
                     1);
    */
} 

register_activation_hook( __FILE__ , 'criar_tabela');

function criar_tabela() {

    global $wpdb;

    $wpdb->query( "CREATE TABLE {$wpdb->prefix}agenda ( 
        id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
        nome VARCHAR(255)NOT NULL, 
        tel BIGINT UNSIGNED NOT NULL)"
    );


}  

function open_config_plugin_menu_sgbd(){

    global $wpdb;

    if(isset($_POST['nome']) && isset ($_POST['tel'])) {
        $wpdb->query("  INSERT INTO {$wpdb->prefix}agenda
                        (nome,tel)
                        VALUES('{$_POST['nome']}, {$_POST['tel']})
                    ");
    }
$contatos = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}agenda");

    require 'grava_db.php';
}


register_deactivation_hook( __FILE__, 'destruir_tabela');

function destruir_tabela () {

    global $wpdb;

    $wpdb->query( "DROP TABLE {$wpdb->prefix}agenda");


}


//var_dump($wpdb);