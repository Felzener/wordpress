<?php

/*
* Plugin Name: Criar item menu Admin e Salvar Config
* Plugin URI: https://bitbucket.org/Felzener
* Description: Exercicio em Aula Criando Item no Menu e Salvando configurações.
* Version: 0.0.1
* Author: Sergio Felzener
* Author URI: https://bitbucket.org/Felzener
* License: CC
*/

add_action('admin_init', 'add_contact');

function add_contact(){

    register_setting('add-contact','add-contact-name');
    register_setting('add-contact','add-contact-email');
    register_setting('add-contact','add-contact-website');

}
add_action('admin_menu', 'new_contact');

function new_contact()
{
    // adiciona um item no menu
    /*
    add_submenu_page('options-general.php', 
                     'Configurações add New Contact',
                     'Configurações add New Contact',
                     'administrator',
                     'config-plugin-menu',
                     'open_add_contact',
                     1);
    */
    add_menu_page(
        'Configurações add New Contact',
        'Add Contact',
        'administrator',
        'add-contact',
        'open_add_contact',
        'dashicons-email-alt'
    );
}

function open_add_contact(){
    require 'plugin_exe_config.php';
}