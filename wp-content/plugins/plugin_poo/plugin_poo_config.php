<div class="wrap">
    <h1>ADD CONTACT</h1>
    <form method="POST" action="options.php">
        <?php
        settings_fields('add-contact');
        do_settings_sections('add-contact');
        ?>
        <label for="add-contact-name-poo">Nome</label>
        <input type="text" id="add-contact-name-poo" name="add-contact-name-poo" value="<?php echo get_option('add-contact-name')?>"><br><br>
        <label for="add-contact-email-poo">E-Mail</label>
        <input type="text" id="add-contact-email-poo" name="add-contact-email-poo" value="<?php echo get_option('add-contact-email')?>"><br><br>
        <label for="add-contact-website-poo">WebSite</label>
        <input type="text" id="add-contact-website-poo" name="add-contact-website-poo" value="<?php echo get_option('add-contact-website')?>"><br><br>
        <?php
        submit_button();
        ?>
    </form>
</div>