<?php
/*
* Plugin Name: Olá Mundo, meu primeiro plugin
* Plugin URI: https://bitbucket.org/Felzener
* Description: Um plugin muito simples que apenas coloca Olá mundo nos posts
* Version: 0.0.1
* Author: Sergio Felzener
* Author URI: https://bitbucket.org/Felzener
* License: CC
*/

// Exemplo de como utilizar um filter
add_filter('login_errors','nova_mensagem_de_erro');

function nova_mensagem_de_erro(){
    return "Credenciais inválidas!";
}

//Fim do exemplo

// Exemplo de como utilizar um action hook
add_action('wp_head', 'add_comentario');

function add_comentario(){

    if(is_single()){
        
        echo "\n\n\n";
        echo '<meta property="og:title" content="' . get_the_title() . '">' . "\n";
        echo '<meta property="og:site_name" content=" ' . wp_title('', false) . '">' . "\n";
        echo '<meta property="og:url" content=" ' . get_permalink(get_the_ID()) . '">' . "\n";
        echo "\n\n\n";
    }
}
//Fim Exemplo de utilização do action hook