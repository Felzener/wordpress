<div class="wrap">
    <h1>Config de Menu</h1>
    <form method="POST" action="options.php">
        <?php
        settings_fields('configs-plugin-menu');
        do_settings_sections('configs-plugin-menu');
        ?>
        <label for="url-api-auth">URL Auth</label>
        <input type="text" id="url-api-auth" name="url-api-auth" value="<?php echo get_option('url-api-auth')?>"><br><br>
        <label for="url-api-endpoint1">URL ENDPoint1</label>
        <input type="text" id="url-api-endpoint1" name="url-api-endpoint1" value="<?php echo get_option('url-api-endpoint1')?>"><br><br>
        <label for="url-api-token">API Token</label>
        <input type="text" id="url-api-token " name="url-api-token" value="<?php echo get_option('url-api-token')?>"><br><br>
        <?php
        submit_button();
        ?>
    </form>
</div>