<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wp' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3]7os47TPg!G:HLY;D!LS-{pXl%~g?2V0N7mr9*>3>0[QN7L,q9kp^Xy3%}&<Llk' );
define( 'SECURE_AUTH_KEY',  'ZfU1udoH}aq>B]3SMx5Tpp(`kR.[YT9]<VTYAbszYgz<[zOCi q(>aI&+X >H.xb' );
define( 'LOGGED_IN_KEY',    '}5lze|qYpR{N)Ym.e-a&urr)gJ$Lco2`|}sXMgki8WvI92c wqCu;0~0;amEVIe&' );
define( 'NONCE_KEY',        'mC{2kE:UfwsgMI $?hUuc<BE^/1<oid;qF_Xn(?Hg[ygsJ6#t#[.93kPGM4Jpk{@' );
define( 'AUTH_SALT',        'Mw$QUg{4F1-(.D K4sd.mEpMV9e>V.`dvjk(FV);v_(Y#9HMF*2e*wlS,`zo1K8H' );
define( 'SECURE_AUTH_SALT', 'V#t>c+73)>BE-*gj5},pKo!!(Rv0Sk|`j)w+k0ok3RFOaeMkp/]*iYZ_JK=uGntP' );
define( 'LOGGED_IN_SALT',   'i|]a{p+Y1{TWw!VWeNeWH3!3j1QGL[{9FzkTqJ=CY#Gy5X0[7;MauY(8$5[H+}~s' );
define( 'NONCE_SALT',       '4CzSc4(>>7((;@nD(m$Ws@J[u;]N/@,NMh5p#k}K->Z94=ai83,c:R7N.o+|;q./' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
