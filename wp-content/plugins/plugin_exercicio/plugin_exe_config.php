<div class="wrap">
    <h1>ADD CONTACT</h1>
    <form method="POST" action="options.php">
        <?php
        settings_fields('add-contact');
        do_settings_sections('add-contact');
        ?>
        <label for="add-contact-name">Nome</label>
        <input type="text" id="add-contact-name" name="add-contact-name" value="<?php echo get_option('add-contact-name')?>"><br><br>
        <label for="add-contact-email">E-Mail</label>
        <input type="text" id="add-contact-email" name="add-contact-email" value="<?php echo get_option('add-contact-email')?>"><br><br>
        <label for="add-contact-website">WebSite</label>
        <input type="text" id="add-contact-website" name="add-contact-website" value="<?php echo get_option('add-contact-website')?>"><br><br>
        <?php
        submit_button();
        ?>
    </form>
</div>